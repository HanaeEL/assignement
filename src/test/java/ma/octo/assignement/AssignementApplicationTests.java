package ma.octo.assignement;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.web.CompteController;
import ma.octo.assignement.web.UtilisateurController;
import ma.octo.assignement.web.VersementController;
import ma.octo.assignement.web.VirementController;

@SpringBootTest
class AssignementApplicationTests {
	
	@Autowired
	private VirementController virementController;

	@Autowired
	private VersementController versementController;
	
	@Autowired
	private CompteController compteController;
	
	@Autowired
	private UtilisateurController utilisateurController;
	
	@Test
	void contextLoads() {
		
		//Assurer la creation des controlleurs
		
		assertThat(virementController).isNotNull();
		
		assertThat(versementController).isNotNull();
		
		assertThat(compteController).isNotNull();
		
		assertThat(utilisateurController).isNotNull();
		
		
	}

}
