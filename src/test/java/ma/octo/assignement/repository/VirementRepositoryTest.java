package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;

import java.math.BigDecimal;
import java.util.Date;


import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@AutoConfigureTestEntityManager
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;
  @Autowired
  private TestEntityManager entityManager;

  @Test
  public void findOne() {
	  
  }

  @Test
  public void findAll() {

  }

  @Test
  public void save() {
	  
	  Utilisateur utilisateur1 = entityManager.persist(new Utilisateur("user1","Male","last1","first1",new Date()));
	  Utilisateur utilisateur2 = entityManager.persist(new Utilisateur("user2","Female","last2","first2",new Date()));
	  Compte compte1 = entityManager.persist(new Compte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1));
	  Compte compte2 = entityManager.persist(new Compte("010000B025001000","RIB2",BigDecimal.valueOf(140000L),utilisateur2));

		Virement v=virementRepository.save(new Virement(
				BigDecimal.TEN,
				new Date(),
				compte1,
				compte2,
				"Assignment 2021"
				)
				);
		
		assertNotNull(v);
		assertThat(v.getId()).isGreaterThan(0);
  }

  @Test
  public void delete() {
  }
}