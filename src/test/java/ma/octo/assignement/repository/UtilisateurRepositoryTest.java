package ma.octo.assignement.repository;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Utilisateur;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@AutoConfigureTestEntityManager
public class UtilisateurRepositoryTest {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	  public void save() {
		Utilisateur utilisateur1 = entityManager.persist(new Utilisateur("user1","Male","last1","first1",new Date()));
		assertNotNull(utilisateurRepository.save(utilisateur1));
		assertThat(utilisateur1.getId()).isGreaterThan(0);
	  }
	
	@Test
	public void findByUsername() {
		String username="user1";
		
		Utilisateur utilisateur1=new Utilisateur("user1","Male","last1","first1",new Date());
		assertThat(utilisateur1.getUsername()).isEqualTo(username);
		assertNotNull(utilisateurRepository.findByUsername(username));
		
	}

}
