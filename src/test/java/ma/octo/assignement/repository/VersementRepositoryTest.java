package ma.octo.assignement.repository;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@AutoConfigureTestEntityManager
public class VersementRepositoryTest {
	
	@Autowired
	 private VersementRepository versementRepository;
	@Autowired
	private TestEntityManager entityManager;
	
	
	@Test
	  public void save() {
		Utilisateur utilisateur1 = entityManager.persist(new Utilisateur("user1","Male","last1","first1",new Date()));
		
		Compte compte1 = entityManager.persist(new Compte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1));
		
		Versement vs = versementRepository.save(new Versement(
				BigDecimal.valueOf(200),
				new Date(),
				utilisateur1.getFirstname()+" "+utilisateur1.getLastname(),
				compte1,
				"motif 1")
				);
		
		assertNotNull(vs);
		assertThat(vs.getId()).isGreaterThan(0);
		  
	  }

}
