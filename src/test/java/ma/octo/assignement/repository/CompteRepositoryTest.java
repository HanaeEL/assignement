package ma.octo.assignement.repository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;

import java.math.BigDecimal;
import java.util.Date;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@AutoConfigureTestDatabase(replace=Replace.NONE)
@AutoConfigureTestEntityManager
public class CompteRepositoryTest {
	
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	  public void save() {
		
		Utilisateur utilisateur1 = entityManager.persist(new Utilisateur("user1","Male","last1","first1",new Date()));
		
		Compte compte1 = compteRepository.save(new Compte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1));
		
		assertNotNull(compte1);
		assertThat(compte1.getId()).isGreaterThan(0);
		  
	  }
	
	@Test
	public void findByNrCompte() {
		String num="020000A000001001";
		
		Utilisateur utilisateur1 = entityManager.persist(new Utilisateur("user1","Male","last1","first1",new Date()));
		
		Compte compte1 = new Compte("020000A000001001","RIB1",BigDecimal.valueOf(200000L),utilisateur1);
		
		assertThat(compte1.getNrCompte()).isEqualTo(num);
		assertNotNull(compteRepository.findByNrCompte(num));
		
	}
	
	@Test
	public void findByRib() {
		String rib="RIB1";
		
		Utilisateur utilisateur1 = entityManager.persist(new Utilisateur("user1","Male","last1","first1",new Date()));
		
		Compte compte1 = new Compte("010000A000001000","RIB1",BigDecimal.valueOf(200000L),utilisateur1);
		
		assertThat(compte1.getRib()).isEqualTo(rib);
		assertNotNull(compteRepository.findByRib(rib));
		
	}
}
