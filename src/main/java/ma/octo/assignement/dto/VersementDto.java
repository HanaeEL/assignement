package ma.octo.assignement.dto;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Compte;

@Component
public class VersementDto {
	private BigDecimal montantVirement;
	private Date dateExecution;
	private String nom_prenom_emetteur;
	private String ribCompteBeneficiaire;
	private String motifVersement;
	
	
	
	public String getNom_prenom_emetteur() {
		return nom_prenom_emetteur;
	}

	public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
		this.nom_prenom_emetteur = nom_prenom_emetteur;
	}

	public BigDecimal getMontantVirement() {
		return montantVirement;
	}
	
	public void setMontantVirement(BigDecimal montantVirement) {
		this.montantVirement = montantVirement;
	}
	
	public Date getDateExecution() {
		return dateExecution;
	}
	
	public void setDateExecution(Date dateExecution) {
		this.dateExecution = dateExecution;
	}
	
	public String getRibCompteBeneficiaire() {
		return ribCompteBeneficiaire;
	}
	
	public void setRibCompteBeneficiaire(String ribCompteBeneficiaire) {
		this.ribCompteBeneficiaire = ribCompteBeneficiaire;
	}
	public String getMotifVersement() {
		return motifVersement;
	}
	public void setMotifVersement(String motifVersement) {
		this.motifVersement = motifVersement;
	}
}
