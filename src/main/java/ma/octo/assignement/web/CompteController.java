package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.NbCompteIsUniqueException;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.service.CompteService;

@RestController(value = "/comptes")
public class CompteController {
	@Autowired
	public CompteService compteService;
	
	@GetMapping("lister_comptes")
    List<CompteDto> loadAllCompte() {
        List<Compte> all = compteService.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return CompteMapper.map(all);
        }
    }
	
	@GetMapping("enregistrer_compte")
    public void save(Compte c) throws NbCompteIsUniqueException {
        if(!compteService.isUniqueNbCompteViolated(c.getNrCompte()) && !compteService.isUniqueRibCompteViolated(c.getRib())) {
        	compteService.save(c);
        }
    }
	
}
