package ma.octo.assignement.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NbCompteIsUniqueException;
import ma.octo.assignement.exceptions.VersementException;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VersementService;
@RestController(value = "/versements")
public class VersementController {
	
	@Autowired
	public VersementService versementService;
	@Autowired
	public CompteService compteService;
	@Autowired
    private AutiService monservice;
	
	@PostMapping("/passerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    public void Versser(@RequestBody  VersementDto versementDto) throws CompteNonExistantException, VersementException, NbCompteIsUniqueException {
		
		
		Compte compte=compteService.getCompteByRib(versementDto.getRibCompteBeneficiaire());
		if(compteService.IsNotEmpty(compte) && versementService.motifIsValide(versementDto.getMotifVersement()) 
				&& versementService.montantIsValide(versementDto.getMontantVirement())) {
			
			compte.setSolde(compte.getSolde().add(versementDto.getMontantVirement()));
			compteService.save(compte);
			
			//creation du versement
			Versement versement = new Versement();
			versement.setDateExecution(versementDto.getDateExecution());
			versement.setMontantVirement(versementDto.getMontantVirement());
			versement.setMotifVersement(versementDto.getMotifVersement());
			versement.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());
			versement.setCompteBeneficiaire(compteService.getCompteByRib(versementDto.getRibCompteBeneficiaire()));
			
			versementService.save(versement);
			
			monservice.auditVersement("Versement depuis " + versementDto.getNom_prenom_emetteur() + " vers " + versementDto.getRibCompteBeneficiaire() 
            + " d'un montant de " + versementDto.getMontantVirement()
                            .toString());
		}
		
	}
	
}
	
	
	
