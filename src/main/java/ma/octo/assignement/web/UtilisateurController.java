package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.exceptions.UsernameIsUniqueException;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.service.UtilisateurService;
@RestController(value = "/utilisateurs")
public class UtilisateurController {
	
	@Autowired
	public UtilisateurService utilisateurService;
	
	@GetMapping("lister_utilisateurs")
    List<UtilisateurDto> loadAllUtilisateur() {
        List<Utilisateur> all = utilisateurService.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return UtilisateurMapper.map(all);
        }
    }
	
	@GetMapping("enregistrer_utilisateur")
    public void save(Utilisateur u) throws UsernameIsUniqueException {
        if(!utilisateurService.isUniqueUsernameViolate(u.getUsername())) {
        	utilisateurService.save(u);
        }
    }
	
	
	
	
}
