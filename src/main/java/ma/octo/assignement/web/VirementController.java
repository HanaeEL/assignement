package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NbCompteIsUniqueException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VirementException;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.AutiService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VirementService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/virements")
public class VirementController {
	
	Logger LOGGER = LoggerFactory.getLogger(AutiService.class);
	
	public static final int MONTANT_MAXIMAL = 10000;
	
    @Autowired
    private CompteService compteService;
    @Autowired
    private VirementService virementService;
    @Autowired
    private AutiService monservice;

    @GetMapping("lister_virements")
    List<VirementDto> loadAll() {
        List<Virement> all = virementService.findAll();
        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return VirementMapper.map(all);
        }
    }
    
    
    
    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException, NbCompteIsUniqueException, VirementException 
    {
    	
    	Compte compteEmetteur = compteService.getCompteByNb(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteService.getCompteByNb(virementDto.getNrCompteEmetteur());
        if(
        		compteService.IsNotEmpty(compteBeneficiaire)
        		&& compteService.IsNotEmpty(compteEmetteur)
        		&& virementService.virementMontantIsValide(virementDto.getMontantVirement())
        		&& virementService.motifIsValide(virementDto.getMotif())
        		&& !compteService.areSameComptes(virementDto.getNrCompteBeneficiaire(), virementDto.getNrCompteEmetteur())
        		)
        {
        	//Virement impossible
        	if(compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
    			new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
    		}else {
    			compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
    	        compteService.save(compteEmetteur);
    	        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
    	        compteService.save(compteBeneficiaire);
    		}
        	
        	//La creation du virement
        	Virement virement=new Virement();
        	virement.setCompteBeneficiaire(compteBeneficiaire);
        	virement.setCompteEmetteur(compteEmetteur);
        	virement.setDateExecution(virementDto.getDate());
        	virement.setMontantVirement(virementDto.getMontantVirement());
        	virement.setMotifVirement(virementDto.getMotif());
        	
        	virementService.save(virement);
        	
        	//Envoyer le message
            monservice.auditVirement("Virement depuis " + compteEmetteur.getNrCompte() + " vers " + compteBeneficiaire.getNrCompte() 
            + " d'un montant de " + virementDto.getMontantVirement()
                            .toString());
        }
    }
}
