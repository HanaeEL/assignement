package ma.octo.assignement.exceptions;

public class UsernameIsUniqueException extends Exception{
private static final long serialVersionUID = 1L;
	
	public UsernameIsUniqueException() {
	  }

	  public UsernameIsUniqueException(String message) {
	    super(message);
	  }
}
