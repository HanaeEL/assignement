package ma.octo.assignement.exceptions;

public class VirementException extends Exception{
private static final long serialVersionUID = 1L;
	
	public VirementException() {
	  }

	  public VirementException(String message) {
	    super(message);
	  }
}
