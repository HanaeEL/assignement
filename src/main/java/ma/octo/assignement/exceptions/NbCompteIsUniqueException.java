package ma.octo.assignement.exceptions;

public class NbCompteIsUniqueException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public NbCompteIsUniqueException() {
	  }

	  public NbCompteIsUniqueException(String message) {
	    super(message);
	  }
}
