package ma.octo.assignement.exceptions;

public class VersementException extends Exception{
	private static final long serialVersionUID = 1L;
	public VersementException() {
	  }

	public VersementException(String message) {
	    super(message);
	  }
}
