package ma.octo.assignement;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.dto.*;
import ma.octo.assignement.mapper.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ma.octo.assignement.web.*;
import ma.octo.assignement.repository.*;
import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {
	@Autowired
	private VirementController virementController;
	@Autowired
	private VersementController versementController;
	@Autowired
	private UtilisateurController utilisateurController;
	@Autowired
	private CompteController compteController;
	@Autowired
	private VirementRepository virementRepository;
	@Autowired
	private VersementRepository versementRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(AssignementApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");
		
		utilisateurController.save(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("user2");
		utilisateur2.setLastname("last2");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");
		
		utilisateurController.save(utilisateur2);
		

		Compte compte1 = new Compte();
		compte1.setNrCompte("020000A000001001");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(200000L));
		compte1.setUtilisateur(utilisateur1);
		
		compteController.save(compte1);
		

		Compte compte2 = new Compte();
		compte2.setNrCompte("020000A000001002");
		compte2.setRib("RIB2");
		compte2.setSolde(BigDecimal.valueOf(140000L));
		compte2.setUtilisateur(utilisateur2);
		
		compteController.save(compte2);
		

		Virement v = new Virement();
		v.setMontantVirement(BigDecimal.TEN);
		v.setCompteBeneficiaire(compte1);
		v.setCompteEmetteur(compte2);
		v.setDateExecution(new Date());
		v.setMotifVirement("Assignment 2021");
		
		
		virementRepository.save(v);
		
		//Entity to Dto
		VirementDto v1=VirementMapper.map(v);
		
		
		//effectuer un virement
		virementController.createTransaction(v1);
		
		Versement vs = new Versement();
		vs.setCompteBeneficiaire(compte1);
		vs.setDateExecution(new Date());
		vs.setMontantVirement(BigDecimal.valueOf(200));
		vs.setMotifVersement("motif 1");
		vs.setNom_prenom_emetteur(utilisateur1.getFirstname()+" "+utilisateur1.getLastname());
		
		versementRepository.save(vs);
		
		//Convertion
		VersementDto versementDto=VersementMapper.map(vs);
		
		
		versementController.Versser(versementDto);
	}
}
