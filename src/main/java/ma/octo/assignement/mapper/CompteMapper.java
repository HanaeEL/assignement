package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.exceptions.NbCompteIsUniqueException;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.UtilisateurService;

public class CompteMapper {
	@Autowired
	private static UtilisateurService utilisateurService;
	@Autowired
	private static CompteService compteService;
	@Autowired
	private static CompteDto compteDto;
	
	public static CompteDto map(Compte compte) {
		compteDto= new CompteDto();
		compteDto.setNrCompte(compte.getNrCompte());
		compteDto.setRib(compte.getRib());
		compteDto.setSolde(compte.getSolde());
		compteDto.setUsername(compte.getUtilisateur().getUsername());
		return compteDto;
	}
	
	public static List<CompteDto> map(List<Compte> comptes) {
		return comptes.stream().map(x->map(x)).collect(Collectors.toList());

	}

}
