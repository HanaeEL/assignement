package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.UtilisateurService;

@Component
public class UtilisateurMapper {


	@Autowired
	private static UtilisateurDto utilisateurDto;
	
	public static UtilisateurDto map(Utilisateur utilisateur) {
		utilisateurDto=new UtilisateurDto();
		utilisateurDto.setBirthdate(utilisateur.getBirthdate());
		utilisateurDto.setFirstname(utilisateur.getFirstname());
		utilisateurDto.setGender(utilisateur.getGender());
		utilisateurDto.setLastname(utilisateur.getLastname());
		utilisateurDto.setUsername(utilisateur.getUsername());
		return utilisateurDto;
	}
	
	public static List<UtilisateurDto> map(List<Utilisateur> utilisateurs) {
		return utilisateurs.stream().map(x->map(x)).collect(Collectors.toList());
	}
	
}
