package ma.octo.assignement.mapper;


import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VirementService;


public class VirementMapper {
	
	private static VirementDto virementDto;
	
    public static VirementDto map(Virement virement) {
    	virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMotif(virement.getMotifVirement());
        //Ajouter Montant
        virementDto.setMontantVirement(virement.getMontantVirement());
        return virementDto;

    }
    
    public static List<VirementDto> map(List<Virement> virements){
    	return virements.stream().map(x -> map(x)).collect(Collectors.toList());
    }
}
