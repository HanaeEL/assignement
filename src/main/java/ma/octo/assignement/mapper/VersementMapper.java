package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VersementService;

public class VersementMapper {
	
	@Autowired
	private static VersementDto versementDto ;
	//Entity to Dto
	public static VersementDto map(Versement versement) {
		versementDto = new VersementDto();
        versementDto.setRibCompteBeneficiaire(versement.getCompteBeneficiaire().getRib());
        versementDto.setDateExecution(versement.getDateExecution());
        versementDto.setMontantVirement(versement.getMontantVirement());
        versementDto.setMotifVersement(versement.getMotifVersement());
        versementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
        
        return versementDto;

    }
	
	public static List<VersementDto> map(List<Versement> versements) {
		return versements.stream().map(x -> map(x)).collect(Collectors.toList());
    }
}
