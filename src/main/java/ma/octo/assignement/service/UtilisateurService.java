package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.exceptions.NbCompteIsUniqueException;
import ma.octo.assignement.exceptions.UsernameIsUniqueException;

@Service
@Transactional
public class UtilisateurService {
	
	@Autowired
	public UtilisateurRepository utilisateurRepository;
	
	public List<Utilisateur> findAll(){
		return utilisateurRepository.findAll();
	}
	
	public boolean isUniqueUsernameViolate(String username) throws UsernameIsUniqueException {
		Utilisateur u=utilisateurRepository.findByUsername(username);
		if(u!=null) {
			throw new UsernameIsUniqueException("username invalide");
		}
		return false;
	}
	
	public void save(Utilisateur utilisateur){
		utilisateurRepository.save(utilisateur);
	}
	
	public Utilisateur getUtilisateurByUserName(String username) {
		return utilisateurRepository.findByUsername(username);
	}
}
