package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.NbCompteIsUniqueException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.VirementException;
import ma.octo.assignement.repository.CompteRepository;

@Service
@Transactional
public class CompteService {
	
	Logger LOGGER = LoggerFactory.getLogger(AutiService.class);
	@Autowired
    private CompteRepository compteRepository;
	
	public List<Compte> findAll(){
		return compteRepository.findAll();
	}
		
	public Compte getCompteByNb(String number) {
		return compteRepository.findByNrCompte(number);
	}
	
	public Compte getCompteByRib(String rib) {
		return compteRepository.findByRib(rib);
	}
	
	
	public boolean IsNotEmpty(Compte compte) throws CompteNonExistantException {
		if(compteRepository.findById(compte.getId()).isEmpty()) {
            throw new CompteNonExistantException("Compte Non existant");
		}
		return true;
	}
	
	public boolean isUniqueNbCompteViolated(String nb) throws NbCompteIsUniqueException{
		
		Compte compte=compteRepository.findByNrCompte(nb);
		if(compte!=null) {
			throw new NbCompteIsUniqueException("Numero de compte existe");
		}
		return false;
	}
	
	public boolean isUniqueRibCompteViolated(String rib) throws NbCompteIsUniqueException{
		
		Compte compte=compteRepository.findByRib(rib);
		if(compte!=null) {
			throw new NbCompteIsUniqueException("compte existe dejas");
		}
		return false;
	}
	
	public boolean areSameComptes(String nbc1,String nbc2) throws NbCompteIsUniqueException{
		Compte c1=compteRepository.findByNrCompte(nbc1);
		Compte c2=compteRepository.findByNrCompte(nbc2);
		if(c1.getNrCompte().equals(c2.getNrCompte())) {
			throw new NbCompteIsUniqueException("Numero de compte existe, changer l'un des comptes");
		}
		return false;
	}
	
	public void save(Compte compte)  {
		compteRepository.save(compte);
	}
}
