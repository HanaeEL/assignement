package ma.octo.assignement.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.VersementException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;

@Service
@Transactional
public class VersementService {

	@Autowired
	public VersementRepository versementRepository;
	@Autowired
	public CompteRepository compteRepository;
	
	public boolean motifIsValide(String motif) throws VersementException {
		if(motif.isEmpty()) {
            throw new VersementException("Motif vide");
		}
		return true;
	}
	
	public boolean montantIsValide(BigDecimal montant) throws VersementException {
		if(montant.intValue()<=0) {
            throw new VersementException("Montant invalide");
		}
		return true;
	}
	
	public void save(Versement versement) {
		versementRepository.save(versement);
	}
}
