package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;

@Service
@Transactional
public class VirementService {
	
	Logger LOGGER = LoggerFactory.getLogger(AutiService.class);
	
	public static final int MONTANT_MAXIMAL = 10000;
	
	@Autowired
    private VirementRepository virementRepository;
	
	
	public List<Virement> findAll(){
		return virementRepository.findAll();
	}
	
	
	public boolean virementMontantIsValide(BigDecimal montant) throws TransactionException {
		if(montant.intValue() <10) {
            throw new TransactionException("Montant minimal de virement non atteint");
		}else if(montant.intValue() > MONTANT_MAXIMAL) {
            throw new TransactionException("Montant maximal de virement dépassé");
		}else if(montant.equals(0)|| montant.equals(null)) {
            throw new TransactionException("Montant vide");
		}
		return true;
	}
	
	public boolean motifIsValide(String motif) throws TransactionException {
		if(motif.isEmpty()) {
            throw new TransactionException("Motif vide");
		}
		return true;
	}
	
	
	
	public void save(Virement virement) {
		virementRepository.save(virement);
	}
}
